## How to start ##
+ On target machine install RabbitMQ service: 
	* with docker (recomended): 
	> docker run -d --hostname my-rabbit --name rabit-mng -p 8080:15672 -p 5672:5672 -p 15672:15672 -v /var/lib/rabbitmq:/var/lib/rabbitmq rabbitmq:3-management

		OR
	
	* manually [link](https://www.rabbitmq.com/install-windows.html) 


+ Install [Python 2.7](https://www.python.org/downloads/release/python-2713/) plugin
> pip install pika

+ Some basic presentation [link](https://www.slideshare.net/ShirishBari/rabbit-mq-introduction-65623925)


### Start/Login to rabbitMQ service ###
borwser:  http://<IpAddress>:15672   (in example _http://localhost:15672_)

user: guest 

password:  guest



## List of examples based on RabbitMQ documentation ##

* [RabbitMQ doc](https://bitbucket.org/lukasz_stefaniszyn/rabbitmq/src/c91e2c1ae2e0cb7d1abe7ce936109ba1ae6b9244/RabbitMQ_in_Depth_v13_MEAP.pdf?at=master)
* [Examples](https://bitbucket.org/lukasz_stefaniszyn/rabbitmq/src/c91e2c1ae2e0cb7d1abe7ce936109ba1ae6b9244/src/?at=master)



## Integration and Performance tests for RabbitMQ ##

To create and execute Integration and Performance tests we use JMeter

* [How to install JMeter and RabbitMQ plugin](https://bitbucket.org/lukasz_stefaniszyn/rabbitmq/src/c91e2c1ae2e0cb7d1abe7ce936109ba1ae6b9244/src/Jmeter/How_To_Install_RabbitMQ_Jmeter_plugin/?at=master)
* [Example test cases](https://bitbucket.org/lukasz_stefaniszyn/rabbitmq/src/c91e2c1ae2e0cb7d1abe7ce936109ba1ae6b9244/src/Jmeter/RPC_Load_Test_1.jmx?at=master&fileviewer=file-view-default)
