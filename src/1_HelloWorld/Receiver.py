# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 23 gru 2016

@author: lucst
'''
import pika


class Reciever(object):
    connection = None;
    channel = None;
    


    def __init__(self, queueAddress, nameOfQueue, ):
        self.__setupConnectionToServer(queueAddress);
        self.__setupConnectionToQueue(nameOfQueue);
        self.__setupLisenerToQueue(nameOfQueue);
        
    
    def __setupConnectionToServer(self, queueAddress):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(queueAddress))
        self.channel = self.connection.channel()

    def __setupConnectionToQueue(self, nameOfQueue):
        self.channel.queue_declare(queue=nameOfQueue)
        
    def __setupLisenerToQueue(self, nameOfQueue):
        self.channel.basic_consume(self.__callback, queue=nameOfQueue, no_ack=True)
        print(' [*] Waiting for messages. To exit press CTRL+C')
        self.channel.start_consuming()

    def __callback(self, ch, method, properties, body):
        print(" [x] Received %r" % body)

    




if __name__ == '__main__':
    rcv = Reciever(queueAddress='192.168.1.17', nameOfQueue='hello');
    