# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 23 gru 2016

@author: lucst
'''
import pika




class Sender(object):
    connection = None;
    channel = None;
    
    def __init__(self, queueAddress, nameOfQueue, ):
        self.__setupConnectionToServer(queueAddress)
        self.__setupConnectionToQueue(nameOfQueue)
    
    def __del__(self):
        try:
            self.connection.close();
        except:
            None

        
    def sendMessageToQueue(self, message, routingKeyName='hello', exchangeName=''):
        self.channel.basic_publish(exchange=exchangeName,routing_key=routingKeyName, body=message)
        print(" [x] Sent 'Hello World!'")


    def __setupConnectionToServer(self, queueAddress):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(queueAddress))
        self.channel = self.connection.channel()
    
    def __setupConnectionToQueue(self, nameOfQueue):
        return self.channel.queue_declare(queue=nameOfQueue)



if __name__ == '__main__':
    
    sender = Sender(queueAddress='192.168.1.17', nameOfQueue='hello')
    sender.sendMessageToQueue(message='Hello World!', );
