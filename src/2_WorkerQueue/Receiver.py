# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 23 gru 2016

@author: lucst
'''
import pika
import time


class Reciever(object):
    connection = None;
    channel = None;
    


    def __init__(self, queueAddress, nameOfQueue, ):
        self.__setupConnectionToServer(queueAddress);
        self.__setupConnectionToQueue(nameOfQueue);
        self.__setupLisenerToQueue(nameOfQueue);

    
    def __setupConnectionToServer(self, queueAddress):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(queueAddress))
        self.channel = self.connection.channel()

    def __setupConnectionToQueue(self, nameOfQueue):
        self.channel.queue_declare(queue=nameOfQueue, durable=True) #RabbitMQ will never lose our queue. In order to do so, we need to declare it as durable
        
    def __setupLisenerToQueue(self, nameOfQueue):
        print(' [*] Waiting for messages. To exit press CTRL+C');
        self.channel.basic_qos(prefetch_count=1); #This tells RabbitMQ not to give more than one message to a worker at a time.
        self.channel.basic_consume(self.__callback, queue=nameOfQueue);
        self.channel.start_consuming();

    def __callback(self, ch, method, properties, body):
        print(" [x] Received %r" % body);
        time.sleep(5);
        print(" [x] Done");
        ch.basic_ack(delivery_tag = method.delivery_tag) #send a proper acknowledgment from the worker, once we're done with a task.

    



if __name__ == '__main__':
    rcv = Reciever(queueAddress='192.168.1.17', nameOfQueue='task_queue');
    