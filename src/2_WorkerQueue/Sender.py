# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 23 gru 2016

@author: lucst
'''
import pika
import sys




class Sender(object):
    connection = None;
    channel = None;
    
    def __init__(self, queueAddress, nameOfQueue, ):
        self.__setupConnectionToServer(queueAddress)
        self.__setupConnectionToQueue(nameOfQueue)
    
    def __del__(self):
        try:
            self.connection.close();
        except:
            None

        
    def sendMessageToQueue(self, message, routingKeyName='task_queue', exchangeName=''):
        self.channel.basic_publish(exchange=exchangeName,
                                   routing_key=routingKeyName, 
                                   body=message, 
                                   properties=pika.BasicProperties(delivery_mode = 2)  # make message persistent
                                   )
        print(" [x] Sent %r" % message)


    def __setupConnectionToServer(self, queueAddress):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(queueAddress))
        self.channel = self.connection.channel()
    
    def __setupConnectionToQueue(self, nameOfQueue):
        self.channel.queue_declare(queue=nameOfQueue, durable=True)



if __name__ == '__main__':
    ##message = ' '.join(sys.argv[1:]) or "Hello World!";
    sender = Sender(queueAddress='192.168.1.17', nameOfQueue='task_queue')
    
    
    for message in map(lambda i : "Text number: " + str(i) , range(1000)):
        print(message);
        sender.sendMessageToQueue(message);
