# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 23 gru 2016
https://www.rabbitmq.com/tutorials/tutorial-three-python.html
@author: lucst
'''
import pika
import time


class Reciever(object):
    connection = None;
    channel = None;
    __exchangeName = '';
    


    def __init__(self, queueAddress, exchangeName):
        self.queueAddress = queueAddress
        self.__exchangeName = exchangeName
        self.__setupConnectionToServer(queueAddress);
        nameOfQueue = self.__setupConnectionToQueue(self.__exchangeName)
        self.__setupLisenerToQueue(nameOfQueue);

    
    def __setupConnectionToServer(self, queueAddress):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(queueAddress))
        self.channel = self.connection.channel()

    def __setupConnectionToQueue(self, exchangeName):
        self.channel.exchange_declare(exchange=exchangeName, exchange_type='fanout');

        result = self.channel.queue_declare(exclusive=True);
        queue_name = result.method.queue;

        self.channel.queue_bind(exchange=exchangeName, queue=queue_name);
        return queue_name;


        
    def __setupLisenerToQueue(self, nameOfQueue):
        print(' [*] Waiting for messages. To exit press CTRL+C');
        self.channel.basic_consume(self.__callback, queue=nameOfQueue, no_ack=True);
        self.channel.start_consuming();

    def __callback(self, ch, method, properties, body):
        print(" [x] Received %r" % body);
        time.sleep(1);
        print(" [x] Done");
        

    



if __name__ == '__main__':
    rcv = Reciever(queueAddress='192.168.1.17', exchangeName='logs');
    