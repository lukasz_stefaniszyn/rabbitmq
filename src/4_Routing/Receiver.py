# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 23 gru 2016
https://www.rabbitmq.com/tutorials/tutorial-three-python.html
@author: lucst
'''
import pika
import time
import sys


class Reciever(object):
    connection = None;
    channel = None;
    __exchangeName = '';
    


    def __init__(self, queueAddress, exchangeName, severity):
        self.queueAddress = queueAddress
        self.__exchangeName = exchangeName
        self.__setupConnectionToServer(queueAddress);
        nameOfQueue = self.__setupConnectionToQueue(self.__exchangeName, severity)
        self.__setupLisenerToQueue(nameOfQueue);

    
    def __setupConnectionToServer(self, queueAddress):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(queueAddress))
        self.channel = self.connection.channel()

    def __setupConnectionToQueue(self, exchangeName, severity):
        self.channel.exchange_declare(exchange=exchangeName, exchange_type='direct');

        result = self.channel.queue_declare(exclusive=True);
        queue_name = result.method.queue;

        self.channel.queue_bind(exchange=exchangeName, queue=queue_name, routing_key=severity);
        return queue_name;


        
    def __setupLisenerToQueue(self, nameOfQueue):
        print(' [*] Waiting for messages. To exit press CTRL+C');
        self.channel.basic_consume(self.__callback, queue=nameOfQueue, no_ack=True);
        try:
            self.channel.start_consuming();
        except pika.exceptions.ConnectionClosed, e:
            print(e);
            

    def __callback(self, ch, method, properties, body):
        print(" [x] Received %r, severity: %r" % (body, method.routing_key));
        time.sleep(1);
        print(" [x] Done");
        

    



if __name__ == '__main__':
    severity = sys.argv[1];
    print(severity)
    rcv = Reciever(queueAddress='192.168.1.17', exchangeName='direct_logs', severity=severity);
    