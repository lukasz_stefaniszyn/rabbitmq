# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 23 gru 2016
https://www.rabbitmq.com/tutorials/tutorial-three-python.html
@author: lucst
'''
import pika
import sys
import random




class Sender(object):
    connection = None;
    channel = None;
    _exchangeName = '';
    
    def __init__(self, queueAddress, exchangeName):
        self._exchangeName = exchangeName;
        self.__setupConnectionToServer(queueAddress)
        self.__setupConnectionToQueue(exchangeName)
    
    def __del__(self):
        try:
            self.connection.close();
        except:
            None

        
    def sendMessage(self, message='', severity='', exchangeName=''):
        print(severity  + ' : ' + message);
        if exchangeName is '': exchangeName = self._exchangeName;
        
        self.channel.basic_publish(exchange=exchangeName,
                                   routing_key=severity, 
                                   body=message, 
                                   )
        print(" [x] Sent %r" % message)


    def __setupConnectionToServer(self, queueAddress):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(queueAddress))
        self.channel = self.connection.channel()
    
    def __setupConnectionToQueue(self, exchangeName=''):
        self.channel.exchange_declare(exchange=exchangeName, exchange_type='direct');
        ##self.channel.queue_declare(queue=nameOfQueue, durable=True)  We don't explicitly create queue. Instead, Exchange will do this for us



if __name__ == '__main__':
    
    sender = Sender(queueAddress='192.168.1.17', exchangeName='direct_logs')
    for message in map(lambda i : "Text number: " + str(i) , range(10)):
        
        severity = random.choice(['info', 'warning', 'error', 'critical'])
        
        sender.sendMessage(message, severity);
