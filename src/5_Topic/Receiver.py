# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 23 gru 2016
https://www.rabbitmq.com/tutorials/tutorial-three-python.html
@author: lucst
'''
import pika
import time
import sys


class Reciever(object):
    connection = None;
    channel = None;
    
    
    __exchangeName = '';
    __routing_key_general = '*';
    


    def __init__(self, queueAddress, exchangeName, routing_key, numberOfQueues=1):
        self.__queueAddress = queueAddress
        self.__exchangeName = exchangeName
        self.__routing_key_general = ''.join((routing_key, '.*'));
        
        self.__setupConnectionToServer(queueAddress);
        self.__setupConnectionToExchange(exchangeName);
        self.__setupConnectionToQueue(exchangeName, routing_key, numberOfQueues)

    
    def __setupConnectionToServer(self, queueAddress):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(queueAddress))
        self.channel = self.connection.channel()


    def __setupConnectionToExchange(self, exchangeName=''):
        self.channel.exchange_declare(exchange=exchangeName, exchange_type='topic', durable=True);

    def __setupConnectionToQueue(self, exchangeName, routing_key, numberOfQueues):
        for number in range(int(numberOfQueues)):
            queueName = '.'.join((routing_key, str(number)))
            self.__createQueue(queueName);
            self.__bindQueueToExchangedBasedOnRoutingKey(exchangeName, queueName)
    def __createQueue(self, nameOfQueue):
        self.channel.queue_declare(queue=nameOfQueue, durable=True)
 
 
    def __bindQueueToExchangedBasedOnRoutingKey(self, exchangeName, nameOfQueue):
        routing_key = self.__routing_key_general;
        self.channel.queue_bind(queue=nameOfQueue, exchange=exchangeName, routing_key=routing_key)

        
    def lisenerToQueue(self, nameOfQueue):
        print(' [*] Waiting for messages. To exit press CTRL+C');
        
        self.channel.basic_consume(self.__callback, queue=nameOfQueue, no_ack=True);
        try:
            self.channel.start_consuming();
        except pika.exceptions.ConnectionClosed, e:
            print(e);
            

    def __callback(self, ch, method, properties, body):
        print(" [x] Received %r, severity: %r" % (body, method.routing_key));
        time.sleep(1);
        print(" [x] Done");
        

    



if __name__ == '__main__':
    #routing_key = sys.argv[1];
    routing_key = 'queue.jira';
    print(routing_key)
    rcv = Reciever(queueAddress='192.168.1.17', exchangeName='topic_queues', routing_key=routing_key, numberOfQueues=4);
    
    rcv.lisenerToQueue(''.join((routing_key,'.0')) );