# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 23 gru 2016
https://www.rabbitmq.com/tutorials/tutorial-three-python.html
@author: lucst
'''
import pika
import sys
import random




class Sender(object):
    connection = None;
    channel = None;
    _exchangeName = '';
    
    def __init__(self, queueAddress, exchangeName):
        self._exchangeName = exchangeName;
        self.__setupConnectionToServer(queueAddress)
        self.__setupConnectionToExchange(exchangeName)
    
    def __del__(self):
        try:
            self.connection.close();
        except:
            None

        
    def sendMessage(self, message='', routing_key='', exchangeName=''):
        print(routing_key  + ' : ' + message);
        if exchangeName is '': exchangeName = self._exchangeName;
        
        self.channel.basic_publish(exchange=exchangeName,
                                   routing_key=routing_key, 
                                   body=message, 
                                   )
        print(" [x] Sent %r" % message)


    def __setupConnectionToServer(self, queueAddress):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(queueAddress))
        self.channel = self.connection.channel()
    


    def __setupConnectionToExchange(self, exchangeName=''):
        self.channel.exchange_declare(exchange=exchangeName, exchange_type='topic', durable=True);



if __name__ == '__main__':
    
    ##Create Exchange and Queues binded together.  
    sender = Sender(queueAddress='192.168.1.17', exchangeName='topic_queues')
    
    ##Fill exchange with messages, message type depends on routing key
    routing_keys = ['queue.jira.#', 'queue.stash.#', 'queue.jenkins.#']
    for message in map(lambda i : "Text number: " + str(i) , range(10)):
        routing_key = random.choice(routing_keys)
        sender.sendMessage(message, routing_key);
