# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 02.01.2017

@author: Lukasz Stefaniszyn <>
'''
import json

class BodyToJsonObject(object):
    
    __jsonObject = None;
    
    def get_json_object(self):
        return self.__jsonObject
    def __set_json_object(self, value):
        self.__jsonObject = value
    jsonObject = property(get_json_object, __set_json_object, None, None)

    
    
    def __init__(self, message):
        self.__jsonObject = json.loads(message);
    
    def getMessageValue(self):
        return int(self.get_json_object()['value']);
