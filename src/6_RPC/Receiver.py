# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 23 gru 2016
https://www.rabbitmq.com/tutorials/tutorial-three-python.html
@author: lucst
'''
import pika

from JsonObject import BodyToJsonObject

class Receiver(object):
    connection = None;
    channel = None;
    
    
    __exchangeName = '';
    __routing_key_general = '#';
    


    def __init__(self, queueAddress, exchangeName, routing_key, numberOfQueues=1):
        self.__queueAddress = queueAddress
        self.__exchangeName = exchangeName
        self.__routing_key_general = ''.join((routing_key, '.#'));
        
        self.__setupConnectionToServer(queueAddress);
        self.__setupConnectionToExchange(exchangeName);
        self.__setupConnectionToQueue(exchangeName, routing_key, numberOfQueues)

    
    def __setupConnectionToServer(self, queueAddress):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(queueAddress))
        self.channel = self.connection.channel()


    def __setupConnectionToExchange(self, exchangeName=''):
        self.channel.exchange_declare(exchange=exchangeName, exchange_type='topic', durable=True);

    def __setupConnectionToQueue(self, exchangeName, routing_key, numberOfQueues):
        for number in range(int(numberOfQueues)):
            queueName = '.'.join((routing_key, str(number)))
            self.__createQueue(queueName);
            self.__bindQueueToExchangedBasedOnRoutingKey(exchangeName, queueName)
    def __createQueue(self, nameOfQueue):
        self.channel.queue_declare(queue=nameOfQueue, durable=True)
 
 
    def __bindQueueToExchangedBasedOnRoutingKey(self, exchangeName, nameOfQueue):
        routing_key = self.__routing_key_general;
        self.channel.queue_bind(queue=nameOfQueue, exchange=exchangeName, routing_key=routing_key)

        
    def lisenerToQueue(self, nameOfQueue):
        print(' [*] Waiting for messages. To exit press CTRL+C');
        self.channel.basic_qos(prefetch_count=1)
        self.channel.basic_consume(self.__callback, queue=nameOfQueue, no_ack=False);
        try:
            self.channel.start_consuming();
        except pika.exceptions.ConnectionClosed, e:
            print(e);
            
    def __callback(self, ch, method, properties, body):
        print(" [x] Received %r, routing_key: %r" % (body, method.routing_key));
        
        n = self.__getValue(properties, body)
        
        print(" [.] fib(%s)" % n)
        response = self.__fib(n)
        response = '{"value":"%s"}' %response;
        print(response);
        
        if properties.correlation_id is None: properties.correlation_id = '0';
        ch.basic_publish(exchange='',
                         routing_key=properties.reply_to,
                         properties=pika.BasicProperties(correlation_id = properties.correlation_id, content_type= 'application/json',),
                         body=str(response))
        ch.basic_ack(delivery_tag = method.delivery_tag)
        print(" [x] Done");
        
    def __getValue(self, properties, body):
        if properties.content_type == 'application/json':
            jsonObject = BodyToJsonObject(message=body)
            n = jsonObject.getMessageValue()
        else:
            n = -1
        return n

    
    def __fib(self, n):
        if n == 0:
            return 0
        elif n == 1:
            return 1
        else:
            return self.__fib(n-1) + self.__fib(n-2);
    



if __name__ == '__main__':
    #routing_key = sys.argv[1];
    routing_key = 'send.jira.sprint';
    print(routing_key)
    rcv = Receiver(queueAddress='192.168.1.17', exchangeName='jira_queue', routing_key=routing_key, numberOfQueues=1);
    
    rcv.lisenerToQueue(''.join((routing_key,'.0')) );