# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 23 gru 2016
https://www.rabbitmq.com/tutorials/tutorial-three-python.html
@author: lucst
'''
import pika
import uuid

from multiprocessing import Process


from JsonObject import BodyToJsonObject

class Sender(object):
    connection = None;
    channel = None;
    _exchangeName = '';
    corr_id = '';
    nameOfCallbackQueue = 'receive.jira.sprint';

    
    
    def __init__(self, queueAddress, exchangeName):
        self._exchangeName = exchangeName;
        self.__setupConnectionToServer(queueAddress)
        self.__setupConnectionToExchange(exchangeName)
        self.__setupCallbackQueue();
    
    def __del__(self):
        print('Closing connection');
        try:
            self.channel.close();
            self.connection.close();
            print('Closed connection');
        except Exception, e:
            print (e);

    
    
    
    def sendMessage(self, message='', routing_key='', exchangeName=''):
        self.response = None
        self.corr_id = str(uuid.uuid4());

        print(self.corr_id  + ' : Fib: ' + message);
        if exchangeName is '': exchangeName = self._exchangeName;
        
        self.channel.basic_publish(exchange=exchangeName,
                                   routing_key=routing_key, 
                                   body=message, 
                                   properties=pika.BasicProperties(
                                       reply_to = self.nameOfCallbackQueue,
                                       content_type= 'application/json',
                                       correlation_id = self.corr_id,
                                        ),
                                   )
        print(" [x] Sent %r" % message)
        
        
        while self.response is None:
            print('Waiting for %s' %self.corr_id);
            #TODO: Implement timeout, do not end up with infinite loop 
            self.connection.process_data_events(1);
        return str(self.response)
        
        


    def __setupConnectionToServer(self, queueAddress):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(queueAddress))
        self.channel = self.connection.channel()
    


    def __setupConnectionToExchange(self, exchangeName=''):
        self.channel.exchange_declare(exchange=exchangeName, exchange_type='topic', durable=True);

    def __setupCallbackQueue(self):
        self.channel.basic_qos(prefetch_count=1); #This tells RabbitMQ not to give more than one message to a worker at a time.
        self.channel.queue_declare(queue=self.nameOfCallbackQueue, durable=True, auto_delete=True);
        self.channel.basic_consume(self.__on_response, no_ack=False,
                                   queue=self.nameOfCallbackQueue)

    def __on_response(self, ch, method, properties, body):
        print(" [x] Received %r, severity: %r, correlation_id: %r" % (body, method.routing_key, properties.correlation_id));
        if self.corr_id == properties.correlation_id:
            ch.basic_ack(delivery_tag = method.delivery_tag) #send a proper acknowledgment from the worker, once we're done with a task.
            self.response = body;
            print(" [x] Done");
            
            result = self.__getResult(properties, body)
            
            print(" [x] OK result: %r, callback_queue: %r, correlation_id: %r" % (result, method.routing_key, properties.correlation_id));
        else:
            print ("%s != %s" %(self.corr_id, properties.correlation_id));
            ch.basic_recover(requeue=True); #give back message to the queue 

    def __getResult(self, properties, body):
        if properties.content_type == 'application/json':
            jsonObject = BodyToJsonObject(message=body)
            result = jsonObject.getMessageValue()
        else:
            result = -1
        return result



        
def runSender(value):
    sender = Sender(queueAddress='192.168.1.17', exchangeName='jira_queue')
    return sender.sendMessage(str(value), routing_key='send.jira.sprint');

if __name__ == '__main__':
    proceses = [];    
    for value in [32, 32, 20, 25]:
        value = '{"value":"%s"}' %(value);
        p = Process(target=runSender, args=(value,))
        proceses.append(p);
    for p in proceses: p.start();    
        
        
    
